<?php

class Listify_Setup {

    public function __construct() {
        if ( ! is_admin() ) {
            return;
        }

        $menus = get_theme_mod( 'nav_menu_locations' );
        $this->theme = wp_get_theme();

        $has_listings = new WP_Query( array( 'post_type' => 'job_listing', 'fields' => 'ids', 'posts_per_page' => 1 ) );

        $this->steps = array();

        $this->steps[ 'install-plugins' ] = array(
            'title' => __( 'Install Required &amp; Recommended Plugins', 'listify' ),
            'completed' => class_exists( 'WP_Job_Manager' ) && class_exists( 'WooCommerce' ),
            'documentation' => array(
                'WP Job Manager' => 'http://listify.astoundify.com/article/228-wp-job-manager',
                'WooCommerce' => 'http://listify.astoundify.com/article/229-woocommerce',
                'Jetpack' => 'http://listify.astoundify.com/article/230-jetpack',
                'Bulk Install' => 'http://listify.astoundify.com/article/320-bulk-install-required-and-recommended-plugins'
            )
        );

        $this->steps[ 'import-content' ] = array(
            'title' => __( 'Import Demo Content', 'listify' ),
            'completed' => $has_listings->have_posts(),
            'documentation' => array(
                'Install Demo Content' => 'http://listify.astoundify.com/article/236-installing-demo-content',
                'Manually Add a Listing' => 'http://listify.astoundify.com/article/245-adding-a-listing',
                'Importing Content (Codex)' => 'http://codex.wordpress.org/Importing_Content',
                'WordPress Importer' => 'https://wordpress.org/plugins/wordpress-importer/'
            )
        );

        $this->steps[ 'import-widgets' ] = array(
            'title' => __( 'Import Widgets', 'listify' ),
            'completed' => is_active_sidebar( 'widget-area-home' ),
            'documentation' => array(
                'Widget Areas' => 'http://listify.astoundify.com/category/352-widget-areas',
                'Widgets' => 'http://listify.astoundify.com/category/199-widgets' 
            )
        );

        $this->steps[ 'setup-menus' ] = array(
            'title' => __( 'Setup Menus', 'listify' ),
            'completed' => isset( $menus[ 'primary' ] ),
            'documentation' => array(
                'Primary Menu' => 'http://listify.astoundify.com/article/250-manage-the-primary-menu',
                'Secondary Menu' => 'http://listify.astoundify.com/article/253-manage-the-secondary-menu',
                'Tertiary Menu' => 'http://listify.astoundify.com/article/254-enable-the-tertiary-navigation',
                'Add a Dropdown' => 'http://listify.astoundify.com/article/252-add-a-dropdown-menu',
                'Add an Avatar' => 'http://listify.astoundify.com/article/251-add-the-avatar-menu-item',
                'Adding Icons' => 'http://listify.astoundify.com/article/257-adding-icons-to-menu-items',
                'Create a Popup' => 'http://listify.astoundify.com/article/255-creating-a-popup-menu-item',
                'Show/Hide Items' => 'http://listify.astoundify.com/article/256-show-different-menus-for-logged-in-or-logged-out',
            )
        );

        $this->steps[ 'setup-homepage' ] = array(
            'title' => __( 'Setup Static Homepage', 'listify' ),
            'completed' => get_option( 'page_on_front' ),
            'documentation' => array(
                'Create Your Homepage' => 'http://listify.astoundify.com/article/261-creating-your-homepage',
                'Reading Settings (codex)' => 'http://codex.wordpress.org/Settings_Reading_Screen'
            )
        );

        $this->steps[ 'setup-widgets' ] = array(
            'title' => __( 'Setup Widgets', 'listify' ),
            'completed' => is_active_sidebar( 'widget-area-home' ),
            'documentation' => array(
                'Widget Areas' => 'http://listify.astoundify.com/category/352-widget-areas',
                'Widgets' => 'http://listify.astoundify.com/category/199-widgets' 
            )
        );

        $this->steps[ 'customize-theme' ] = array(
            'title' => __( 'Customize', 'listify' ),
            'completed' => get_option( 'theme_mods_listify' ),
            'documentation' => array(
                'Appearance' => 'http://listify.astoundify.com/category/334-appearance',
                'Booking Services' => 'http://listify.astoundify.com/category/455-booking-service-integration',
                'Child Themes' => 'http://listify.astoundify.com/category/209-child-themes',
                'Translations' => 'http://listify.astoundify.com/category/210-translations'
            )
        );

        $this->steps[ 'support-us' ] = array(
            'title' => __( 'Get Involved', 'listify' ),
            'completed' => 'n/a',
            'documentation' => array(
                'Leave a Positive Review' => 'http://bit.ly/rate-listify',
                'Contribute Your Translation' => 'http://bit.ly/translate-listify'
            )
        );

        add_action( 'admin_menu', array( $this, 'add_page' ), 100 );
        add_action( 'admin_menu', array( $this, 'add_meta_boxes' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'admin_css' ) );
    }

    public function add_page() {
        add_submenu_page( 'themes.php', __( 'Listify Setup', 'listify' ), __( 'Setup Guide', 'listify' ), 'manage_options', 'listify-setup', array( $this, 'output' ) );
    }

    public function admin_css() {
        $screen = get_current_screen();

        if ( 'appearance_page_listify-setup' != $screen->id ) {
            return;
        }

        wp_enqueue_style( 'listify-setup', get_template_directory_uri() . '/inc/setup/style.css' );
    }

    public function add_meta_boxes() {
        foreach ( $this->steps as $step => $info ) {
            $info = array_merge( array( 'step' => $step ), $info );
            add_meta_box( $step , $info[ 'title' ], array( $this, 'step_box' ), 'listify_setup_steps', 'normal', 'high', $info );
        }
    }

    public function step_box( $object, $metabox ) {
        $args = $metabox[ 'args' ];
    ?>
        <?php if ( $args[ 'completed' ] == true  ) { ?>
            <div class="is-completed"><?php _e( 'Completed!', 'listify' ); ?></div>
        <?php } elseif ( $args[ 'completed' ] == false && 'n/a' != $args[ 'completed' ] ) { ?>
            <div class="not-completed"><?php _e( 'Incomplete', 'listify' ); ?></div>
        <?php } ?>

        <?php include ( get_template_directory() . '/inc/setup/steps/' . $args[ 'step' ] . '.php' ); ?>

        <?php if ( 'Get Involved' != $args[ 'title' ] ) : ?> 
        <hr />
        <p><?php _e( 'You can read more and watch helpful video tutorials below:', 'listify' ); ?></p>
        <?php endif; ?>

        <p>
            <?php foreach ( $args[ 'documentation' ] as $title => $url ) { ?>
            <a href="<?php echo esc_url( $url ); ?>" class="button button-secondary"><?php echo esc_attr( $title ); ?></a>&nbsp;
            <?php } ?>
        </p>
    <?php
    }

    public function output() {
    ?>
<div class="wrap about-wrap listify-setup">
    <?php $this->welcome(); ?>
    <?php $this->links(); ?>
</div>

<div id="poststuff" class="wrap listify-steps" style="margin: 25px 40px 0 20px">
    <?php $this->steps(); ?>
</div>

<script>!function(e,o,n){window.HSCW=o,window.HS=n,n.beacon=n.beacon||{};var t=n.beacon;t.userConfig={},t.readyQueue=[],t.config=function(e){this.userConfig=e},t.ready=function(e){this.readyQueue.push(e)},o.config={modal: true, docs:{enabled:!0,baseUrl:"//astoundify-listify.helpscoutdocs.com/"},contact:{enabled:!1,formId:"f830bbd3-6615-11e5-8846-0e599dc12a51"}};var r=e.getElementsByTagName("script")[0],c=e.createElement("script");c.type="text/javascript",c.async=!0,c.src="https://djtflbt20bdde.cloudfront.net/",r.parentNode.insertBefore(c,r)}(document,window.HSCW||{},window.HS||{});</script>
    <?php
    }

    public function welcome() {
    ?>
<h1><?php printf( __( 'Welcome to Listify %s', 'listify' ), $this->theme->Version ); ?></h1>
<p class="about-text"><?php printf( __( 'The last directory you will ever buy. Use the steps below to finish setting up your new website. If you have more questions please <a href="%s">review the documentation</a>.', 'listify' ), 'http://listify.astoundify.com' ); ?></p>
<div class="listify-badge"><img src="<?php echo get_template_directory_uri(); ?>/images/listify-banner-welcome.jpg" width="140" alt="" /></div>
    <?php
    }

    public function links() {
    ?>
<p class="helpful-links">
    <a href="http://listify.astoundify.com" class="button button-primary js-trigger-documentation"><?php _e( 'Search Documentation', 'listify' ); ?></a>&nbsp;
    <a href="https://astoundify.com/go/astoundify-support/" class="button button-secondary"><?php _e( 'Submit a Support Ticket', 'listify' ); ?></a>&nbsp;
</p>
<script>
    jQuery(document).ready(function($) {
        $('.js-trigger-documentation').click(function(e) {
            e.preventDefault();
            HS.beacon.open();
        });
    });
</script>
    <?php
    }

    public function steps() {
        do_accordion_sections('listify_setup_steps', 'normal', null );
    }
}

$GLOBALS[ 'listify_setup' ] = new Listify_Setup;
