<?php
/**
 * Control group for Color Schemes
 *
 * @since Listify 1.3.0
 */

class Listify_Customizer_Control_Group_Style_Kit {

    public function __construct() {
        $this->group_id = 'style-kit';

        $this->groups = apply_filters( 'listify_control_group_' . $this->group_id, array(
            'default' => array(
                'title' => __( 'Original', 'listify' ),
                'controls' => array(
                    'color-scheme' => 'default',
                    'font-pack' => 'montserrat',
                    'color-content-background' => '#ffffff',
                    'color-content-border' => '#ffffff',
                    'content-box-style' => 'default',
                    'content-button-style' => 'default'
                )
            ),
            'iced-coffee' => array(
                'title' => __( 'Iced Coffee', 'listify' ),
                'controls' => array(
                    'color-scheme' => 'iced-coffee',
                    'font-pack' => 'lato',
                    'color-content-background' => '#ffffff',
                    'color-content-border' => '#ffffff',
                    'content-box-style' => 'shadow',
                    'content-button-style' => 'solid'
                )
            ),
            'radical-red' => array(
                'title' => __( 'Radical Red', 'listify' ),
                'controls' => array(
                    'color-scheme' => 'radical-red',
                    'font-pack' => 'karla',
                    'color-content-background' => '#ffffff',
                    'color-content-border' => '#dcdcdc',
                    'content-box-style' => 'minimal',
                    'content-button-style' => 'solid',
                )
            ),
            'green-flash' => array(
                'title' => __( 'Green Flash', 'listify' ),
                'controls' => array(
                    'color-scheme' => 'green-flash',
                    'font-pack' => 'varela-round',
                    'color-content-background' => '#fcfdff',
                    'color-content-border' => '#e3eaf4',
                    'content-box-style' => 'default',
                    'content-button-style' => 'outline',
                )
            ),
            'ultra-dark' => array(
                'title' => __( 'Ultra Dark', 'listify' ),
                'controls' => array(
                    'color-scheme' => 'ultra-dark',
                    'font-pack' => 'karla',
                    'color-content-background' => '#292932',
                    'color-content-border' => '#292932',
                    'content-box-style' => 'minimal',
                    'content-button-style' => 'solid',
                )
            )
        ) );
    }
}
