<?php
/**
 * Color Panel
 *
 * @since Listify 1.3.0
 */

class Listify_Customizer_Panel_Colors extends Listify_Customizer_Panel {

    public function __construct() {
        parent::__construct( array(
            'id' => 'colors',
            'title' => __( 'Colors', 'listify' ),
            'priority' => 2
        ) );

        add_action( 'customize_register', array( $this, 'remove_section' ), 40 );
        add_action( 'customize_register', array( $this, 'move_controls' ), 40 );

        $this->sections = array( 
            'color-scheme' => array(
                'title' => __( 'Color Scheme', 'listify' ),
                'controls' => array(
                    'color-scheme' => array(
                        'label' => __( 'Color Scheme', 'listify' ),
                        'type' => 'Listify_Customize_Color_Scheme_Control',
                        'schemes' => listify_get_control_group( 'color-scheme' )
                    )
                )
            ),
            'color-global' => array(
                'title' => __( 'Global', 'listify' ),
                'controls' => array(
                    'color-primary' => array(
                        'label' => __( 'Primary Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-accent' => array(
                        'label' => __( 'Accent Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-body-text' => array(
                        'label' => __( 'Body Text Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-link' => array(
                        'label' => __( 'Link Text Color', 'listify' ),
                        'type' => 'WP_Customize_Color_Control',
                    )
                )
            ),
            'color-header' => array(
                'title' => __( 'Header/Navigation', 'listify' ),
                'controls' => array(
                    'color-navigation-text' => array(
                        'label' => __( 'Navigation Text Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-header-background' => array(
                        'label' => __( 'Header Background Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-secondary-navigation-text' => array(
                        'label' => __( 'Secondary Navigation Text Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-secondary-navigation-background' => array(
                        'label' => __( 'Secondary Navigation Background Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-tertiary-navigation-text' => array(
                        'label' => __( 'Tertiary Navigation Text Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-tertiary-navigation-background' => array(
                        'label' => __( 'Tertiary Navigation Background Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                )
            ),
            'content-inputs' => array(
                'title' => __( 'Inputs', 'listify' ),
                'controls' => array(
                    'color-input-text' => array(
                        'label' => __( 'Input Box Text Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-input-background' => array(
                        'label' => __( 'Input Box Background Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-input-border' => array(
                        'label' => __( 'Input Box Border Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    )
                )
            ),
            'content-box' => array(
                'title' => __( 'Content Box & Widgets', 'listify' ),
                'controls' => array(
                    'color-content-background' => array(
                        'label' => __( 'Content Box Background Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-content-border' => array(
                        'label' => __( 'Content Box Border Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    ),
                    'color-content-accent' => array(
                        'label' => __( 'Content Box Accent Color', 'listify' ),
                        'type'    => 'WP_Customize_Color_Control'
                    )
                )
            ),
            'color-footer' => array(
                'title' => __( 'Footer', 'listify' ),
                'controls' => array(
                    'color-as-seen-on-background' => array(
                        'label' => __( '"As Seen On" Background Color', 'listify' ),
                        'type' => 'WP_Customize_Color_Control'
                    ),
                    'color-cta-text' => array(
                        'label' => __( 'Call to Action Text Color', 'listify' ),
                        'type' => 'WP_Customize_Color_Control'
                    ),
                    'color-cta-background' => array(
                        'label' => __( 'Call to Action Background Color', 'listify' ),
                        'type' => 'WP_Customize_Color_Control'
                    ),
                    'color-footer-widgets-text' => array(
                        'label' => __( 'Footer Widgets Text Color', 'listify' ),
                        'type' => 'WP_Customize_Color_Control'
                    ),
                    'color-footer-widgets-background' => array(
                        'label' => __( 'Footer Widgets Background Color', 'listify' ),
                        'type' => 'WP_Customize_Color_Control'
                    ),
                    'color-footer-text' => array(
                        'label' => __( 'Footer Text Color', 'listify' ),
                        'type' => 'WP_Customize_Color_Control'
                    ),
                    'color-footer-background' => array(
                        'label' => __( 'Footer Background Color', 'lisify' ),
                        'type' => 'WP_Customize_Color_Control'
                    )
                )
            ),
        );
    }

    public function remove_section( $wp_customize ) {
        $wp_customize->remove_section( 'colors' );
    }

    public function move_controls( $wp_customize ) {
        $wp_customize->get_control( 'header_textcolor' )->section = 'color-header';

        $wp_customize->get_control( 'background_color' )->section = 'color-global';
        $wp_customize->get_control( 'background_color' )->label = __( 'Page Background Color', 'listify' );
    }

}
