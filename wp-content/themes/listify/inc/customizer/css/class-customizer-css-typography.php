<?php

class Listify_Customizer_CSS_Typography {

    public function __construct() {
        $this->css = listify_customizer()->css;

        add_action( 'listify_output_customizer_css', array( $this, 'typography' ) );
    }

    public function typography() {
        /**
         * Body
         */
        $body_font_family = listify_theme_mod( 'typography-body-font-family' );
        $body_font_weight = listify_theme_mod( 'typography-body-font-weight' );
        $body_font_size = listify_theme_mod( 'typography-body-font-size' );
        $body_line_height = listify_theme_mod( 'typography-body-line-height' );

        $selectors = array(
            ':not(.wp-core-ui) button',
            'body',
            'input',
            'select',
            'textarea'
        );

        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-family' => listify_customizer()->fonts->get_font_family( $body_font_family ),
                'font-weight' => esc_attr( $body_font_weight )
            )
        ) );

        // size and line height only on desktop
        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-size' => absint( $body_font_size ) . 'px',
                'line-height' => listify_customizer()->fonts->get_line_height( $body_line_height )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        /**
         * Page Headings
         */
        $page_headings_font_family = listify_theme_mod( 'typography-page-headings-font-family' );
        $page_headings_font_weight = listify_theme_mod( 'typography-page-headings-font-weight' );
        $page_headings_font_size = listify_theme_mod( 'typography-page-headings-font-size' );
        $page_headings_line_height = listify_theme_mod( 'typography-page-headings-line-height' );

        $selectors = array(
            '.page-title',
            '.job_listing-title',
            '.popup-title',
            '.homepage-cover .home-widget-title'
        );

        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-family' => listify_customizer()->fonts->get_font_family( $page_headings_font_family ),
                'font-weight' => esc_attr( $page_headings_font_weight )
            )
        ) );

        // size and line height only on desktop
        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-size' => absint( $page_headings_font_size ) . 'px',
                'line-height' => listify_customizer()->fonts->get_line_height( $page_headings_line_height )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.homepage-cover .home-widget-title'
            ),
            'declarations' => array(
                'font-size' => absint( $page_headings_font_size * 1.5 ) . 'px'
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        /**
         * Content Headings
         */
        $content_headings_font_family = listify_theme_mod( 'typography-content-headings-font-family' );
        $content_headings_font_weight = listify_theme_mod( 'typography-content-headings-font-weight' );
        $content_headings_font_size = listify_theme_mod( 'typography-content-headings-font-size' );
        $content_headings_line_height = listify_theme_mod( 'typography-content-headings-line-height' );

        $selectors = array(
            '.widget-title',
            '.comment-reply-title',
        );

        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-family' => listify_customizer()->fonts->get_font_family( $content_headings_font_family ),
                'font-weight' => esc_attr( $content_headings_font_weight )
            )
        ) );

        // size and line height only on desktop
        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-size' => absint( $content_headings_font_size ) . 'px',
                'line-height' => listify_customizer()->fonts->get_line_height( $content_headings_line_height )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        /**
         * Homepage Headings
         */
        $home_headings_font_family = listify_theme_mod( 'typography-home-headings-font-family' );
        $home_headings_font_weight = listify_theme_mod( 'typography-home-headings-font-weight' );
        $home_headings_font_size = listify_theme_mod( 'typography-home-headings-font-size' );
        $home_headings_line_height = listify_theme_mod( 'typography-home-headings-line-height' );

        $selectors = array(
            '.home-widget-title',
        );

        $extra_selectors = array_merge( $selectors, array(
            '.callout-feature-content h2',
            'home-feature-title h2'
        ) );

        $this->css->add( array(
            'selectors' => $extra_selectors,
            'declarations' => array(
                'font-family' => listify_customizer()->fonts->get_font_family( $home_headings_font_family ),
                'font-weight' => esc_attr( $home_headings_font_weight )
            )
        ) );

        // size and line height only on desktop
        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-size' => absint( $home_headings_font_size ) . 'px',
                'line-height' => listify_customizer()->fonts->get_line_height( $home_headings_line_height )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        /**
         * Homepage Descriptions
         */
        $home_description_font_family = listify_theme_mod( 'typography-home-description-font-family' );
        $home_description_font_weight = listify_theme_mod( 'typography-home-description-font-weight' );
        $home_description_font_size = listify_theme_mod( 'typography-home-description-font-size' );
        $home_description_line_height = listify_theme_mod( 'typography-home-description-line-height' );

        $selectors = array(
            '.home-widget-description',
        );

        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-family' => listify_customizer()->fonts->get_font_family( $home_description_font_family ),
                'font-weight' => esc_attr( $home_description_font_weight )
            )
        ) );

        // size and line height only on desktop
        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-size' => absint( $home_description_font_size ) . 'px',
                'line-height' => listify_customizer()->fonts->get_line_height( $home_description_line_height )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        /**
         * Buttons
         */
        $buttons_font_family = listify_theme_mod( 'typography-button-font-family' );
        $buttons_font_weight = listify_theme_mod( 'typography-button-font-weight' );
        $buttons_font_size = listify_theme_mod( 'typography-button-font-size' );
        $buttons_line_height = listify_theme_mod( 'typography-button-line-height' );

        $selectors = array(
            'button:not([role="presentation"])',
            'input[type="button"]',
            'input[type="reset"]',
            'input[type="submit"]',
            '.button'
        );

        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-family' => listify_customizer()->fonts->get_font_family( $buttons_font_family ),
                'font-weight' => esc_attr( $buttons_font_weight )
            )
        ) );

        // size and line height only on desktop
        $this->css->add( array(
            'selectors' => $selectors,
            'declarations' => array(
                'font-size' => absint( $buttons_font_size ) . 'px',
                'line-height' => listify_customizer()->fonts->get_line_height( $buttons_line_height )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

    }

}
