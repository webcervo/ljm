<?php
/**
 * Listings Panel
 *
 * @since Listify 1.3.0
 */

class Listify_Customizer_Panel_Listings extends Listify_Customizer_Panel {

    public function __construct() {
        global $listify_strings, $listify_job_manager;

        parent::__construct( array(
            'id' => 'listings',
            'title' => $listify_strings->label( 'plural' ),
            'priority' => 5
        ) );

        add_filter( 'listify_customizer_scripts_data', array( $this, 'customizer_scripts' ) );

        $this->sections = apply_filters( 'listify_customizer_panel_listings', array(
            'labels' => array(
                'title' => __( 'Labels & Behavior', 'listify' ),
                'controls' => array(
                    'label-singular' => array(
                        'label' => __( 'Singular Label', 'listify' )
                    ),
                    'label-plural' => array(
                        'label' => __( 'Plural Label', 'listify' )
                    ),
                    'region-bias' => array(
                        'label' => __( 'Base Country', 'listify' ),
                        'type' => 'select',
                        'description' => __( 'This controls autocomplete priority, distance units, and more.', 'listify' ),
                        'choices' => listify_has_integration( 'woocommerce' ) ? array_merge( array( '' => __( 'None', 'listify' ) ), WC()->countries->get_countries() ) : array()
                    ),
                    'social-association' => array(
                        'label' => __( 'Social Profiles', 'listify' ),
                        'type' => 'select',
                        'description' => __( 'If associated with a listing the fields will appear on the submission form', 'listify' ),
                        'choices' => array(
                            'listing' => __( 'Associate with listing', 'listify' ),
                            'user' => __( 'Associate with user', 'listify' )
                        )
                    ),
                    'custom-submission' => array(
                        'label' => __( 'Use "directory" submission fields', 'listify' ),
                        'type' => 'checkbox',
                    ),
                    'gallery-comments' => array(
                        'label' => __( 'Allow comments on gallery images', 'listify' ),
                        'type' => 'checkbox',
                    ),
                    'categories-only' => array(
                        'label' => __( 'Use categories only', 'listify' ),
                        'type' => 'checkbox',
                        'description' => __( 'Categories will be used to create map markers, and types will be hidden from all other areas. Categories must be enabled in Listings > Settings. <br /><br />Refresh this page in your browser after saving.', 'listify' )
                    )
                )
            ),
            'listing-archive' => array(
                'title' => __( 'Results Layout', 'listify' ),
                'controls' => array(
                    'listing-archive-output' => array(
                        'label' => __( 'Display', 'listify' ),
                        'type' => 'select',
                        'choices' => array(
                            'results' => __( 'Results Only', 'listify' ),
                            'map-results' => __( 'Map & Results', 'listify' )
                        )
                    ),
                    'listing-archive-map-position' => array(
                        'label' => __( 'Map Position', 'listify' ),
                        'type'  => 'select',
                        'choices' => array(
                            'side' => __( 'Side (fixed)', 'listify' ),
                            'top'  => __( 'Top', 'listify' )
                        )
                    ),
                    'listing-archive-display-style' => array(
                        'label' => __( 'Display Style', 'listify' ),
                        'type'  => 'select',
                        'choices' => array(
                            'grid' => __( 'Grid', 'listify' ),
                            'list' => __( 'List', 'listify' )
                        )
                    )
                )
            ),
            'listing-single' => array(
                'title' => sprintf( __( '%s Layout', 'listify' ), $listify_strings->label( 'singular' ) ),
                'controls' => array(
                    // 'listing-header-style' => array(
                    //     'label' => __( 'Header Style', 'listify' ),
                    //     'type' => 'select',
                    //     'choices' => array(
                    //         'default' => __( 'Default', 'listify' ),
                    //         'gallery' => __( 'Gallery Slider', 'listify' ),
                    //         'map' => __( 'Map', 'listify' )
                    //     )
                    // ),
                    'listing-single-sidebar-position' => array(
                        'label' => __( 'Sidebar Position', 'listify' ),
                        'type' => 'select',
                        'choices' => array(
                            'none' => __( 'None', 'listify' ),
                            'left' => __( 'Left', 'listify' ),
                            'right' => __( 'Right', 'listify' ),
                        )
                    )
                )
            ),
            'map-appearance' => array(
                'title' => __( 'Map Appearance', 'listify' ),
                'controls' => array(
                    'map-appearance-scheme' => array(
                        'label' => __( 'Color Scheme', 'listify' ),
                        'type' => 'Listify_Customize_Color_Scheme_Control',
                        'priority' => 0,
                        'schemes' => $listify_job_manager->map->schemes->get_color_schemes(),
                        'full_schemes' => true,
                        'description' => sprintf( __( 'Some color schemes may show/hide extra information. Please <a href="#">read more</a> about creating a custom scheme.', 'listify' ), 'http://listify.astoundify.com/article/805-create-a-custom-map-color-scheme' )
                    )
                )
            ),
            'map-behavior' => array(
                'title' => __( 'Map Settings', 'listify' ),
                'controls' => array(
                    'map-behavior-api-key' => array(
                        'label' => __( 'Google Maps API Key (optional)', 'listify' )
                    ),
                    'map-behavior-trigger' => array(
                        'label' => __( 'Info Bubble Trigger', 'listify' ),
                        'type' => 'select',
                        'choices' => array(
                            'mouseover' => __( 'Hover', 'listify' ),
                            'click' => __( 'Click', 'listify' )
                        )
                    ),
                    'map-behavior-clusters' => array(
                        'label' => __( 'Use Clusters', 'listify' ),
                        'type' => 'checkbox',
                    ),
                    'map-behavior-grid-size' => array(
                        'label' => __( 'Cluster Grid Size (px)', 'listify' )
                    ),
                    'map-behavior-autofit' => array(
                        'label' => __( 'Autofit on load', 'listify' ),
                        'type' => 'checkbox'
                    ),
                    'map-behavior-center' => array(
                        'label' => __( 'Default Center Coordinate', 'listify' )
                    ),
                    'map-behavior-zoom' => array(
                        'label' => __( 'Default Zoom Level', 'listify' ),
                        'type' => 'select',
                        'choices' => $this->map_zoom_ints()
                    ),
                    'map-behavior-max-zoom' => array(
                        'label' => __( 'Max Zoom In Level', 'listify' ),
                        'type' => 'select',
                        'choices' => $this->map_zoom_ints()
                    ),
                    'map-behavior-max-zoom-out' => array(
                        'label' => __( 'Max Zoom Out Level', 'listify' ),
                        'type' => 'select',
                        'choices' => $this->map_zoom_ints()
                    ),
                    'map-behavior-scrollwheel' => array(
                        'label' => __( 'Zoom with Scrollwheel', 'listify' ),
                        'type' => 'checkbox'
                    )
                )
            ),
            'marker-appearance' => array(
                'title' => __( 'Map Marker Colors', 'listify' ),
                'controls' => array()
            ),
        ) );

        if ( ! listify_has_integration( 'facetwp' ) ) {
            $this->sections[ 'map-behavior' ][ 'controls' ][ 'map-behavior-search-min' ] = array(
                'label' => sprintf( __( 'Search Radius Min (%s)', 'listify' ), $listify_job_manager->map->template->unit() )
            );

            $this->sections[ 'map-behavior' ][ 'controls' ][ 'map-behavior-search-max' ] = array(
                'label' => sprintf( __( 'Search Radius Max (%s)', 'listify' ), $listify_job_manager->map->template->unit() )
            );

            $this->sections[ 'map-behavior' ][ 'controls' ][ 'map-behavior-search-default' ] = array(
                'label' => sprintf( __( 'Search Radius Default (%s)', 'listify' ), $listify_job_manager->map->template->unit() )
            );
        }

        // set colors for top level taxonomy (used on map)
        $taxonomy = listify_get_top_level_taxonomy();	
		$terms = listify_get_terms( $taxonomy );

        if ( ! is_wp_error( $terms ) && apply_filters( 'listify_customizer_marker_colors', true ) ) {
            foreach ( $terms as $term ) {
                $this->sections[ 'marker-appearance' ][ 'controls' ][ 'marker-color-' . $term->term_id ] = array(
                    'label' => sprintf( __( '%s Marker Color', 'listify' ), $term->name ),
                    'type' => 'WP_Customize_Color_Control'
                );
            }
        }

        // set icons for each taxonomy
        // $taxonomies = get_object_taxonomies( 'job_listing', 'objects' );
        $taxonomies = array( get_taxonomy( listify_get_top_level_taxonomy() ) );

        $tags = get_taxonomy( 'job_listing_tag' );

        if ( $tags ) {
            $taxonomies[] = $tags;
        }

		$taxonomies = apply_filters( 'listify_get_customizer_taxonomies_for_icons', $taxonomies );

        if ( ! empty( $taxonomies ) ) {
            foreach ( $taxonomies as $key => $taxonomy ) {
                if ( is_wp_error( $taxonomy ) ) {
                    continue;
                }

                $controls = array();
				$terms = listify_get_terms( $taxonomy->name );

                if ( empty( $terms ) ) {
                    continue;
                }

                $controls[ 'listing-icon-desc-' . $taxonomy->name ] = array(
                    'type' => 'Listify_Customize_Description_Control',
                    'label' => __( 'Looking for the perfect icon? Visit the <a href="http://ionicons.com" target="_blank">Ionicons website</a> to easily browse available icons.', 'listify' )
                );

                foreach ( $terms as $term ) {
                    $controls[ 'listings-' . $taxonomy->name . '-' . $term->slug . '-icon' ] = array(
                        'label' => $term->name,
                        'type' => 'select',
                        'choices' => array( 'placeholder' => __( 'Loading...', 'listify' ) )
                    );
                }

                $this->sections[ 'listings-' . $taxonomy->name . '-icons' ] = array(
                    'title' => sprintf( __( '%s Icons', 'listify' ), $taxonomy->label ),
                    'controls' => $controls
                );
            }
        }

    }

    public function map_zoom_ints() {
        return array( '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18' );
    }

    public function customizer_scripts( $data ) {
        $data[ 'icons' ] = array(
            'options' => listify_customizer()->output->get_regex_theme_mods( 'icon' ),
            'choices' => listify_customizer()->icons->all_icon_choices()
        );

        return $data;
    }

}
