<?php
/**
 * The template for displaying pages.
 *
 * @package Listify
 */

get_header(); ?>

    <?php do_action( 'listify_page_before' ); ?>

    <div id="primary" class="container">
        <div class="row content-area">

            <main id="main" class="site-main col-sm-12" role="main">

                <?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; ?>

            </main>

        </div>
    </div>


<?php get_footer(); ?>
