<?php wp_enqueue_script( 'wp-job-manager-ajax-filters' ); ?>

<?php do_action( 'job_manager_job_filters_before', $atts ); ?>

<form class="job_filters">
	<?php do_action( 'job_manager_job_filters_start', $atts ); ?>

	<div class="search_jobs">
		<?php do_action( 'job_manager_job_filters_search_jobs_start', $atts ); ?>

		<div class="search_keywords">
			<label for="search_keywords"><?php _e( 'Keywords', 'wp-job-manager' ); ?></label>
			<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'Keywords', 'wp-job-manager' ); ?>" value="<?php echo esc_attr( $keywords ); ?>" />
		</div>

		<div class="search_location">
			<label for="search_location"><?php _e( 'Location', 'wp-job-manager' ); ?></label>
			<input type="text" name="search_location" id="search_location" placeholder="<?php esc_attr_e( 'Location', 'wp-job-manager' ); ?>" value="<?php echo esc_attr( $location ); ?>" />
		</div>

		<?php if ( $categories ) : ?>
			<?php foreach ( $categories as $category ) : ?>
				<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( $category ); ?>" />
			<?php endforeach; ?>
		<?php elseif ( $show_categories && ! is_tax( 'job_listing_category' ) && get_terms( 'job_listing_category' ) ) : ?>
			<div class="search_categories">
				<label for="search_categories"><?php _e( 'Category', 'wp-job-manager' ); ?></label>
				<?php if ( $show_category_multiselect ) : ?>
					<?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
				<?php else : ?>
					<?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'show_option_all' => __( 'Any category', 'wp-job-manager' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) ); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if ( $mrcs ) : ?>
			<?php foreach ( $mrcs as $category ) : ?>
				<input type="hidden" name="search_mrcs[]" value="<?php echo sanitize_title( $category ); ?>" />
			<?php endforeach; ?>
		<?php elseif ( $show_mrcs && ! is_tax( 'mrc' ) && get_terms( 'mrc' ) ) : ?>
			<div class="search_mrcs">
				<label for="search_mrcs"><?php _e( 'MRC', 'wp-job-manager' ); ?></label>
				<?php if ( $show_category_multiselect ) : ?>
					<?php job_manager_dropdown_mrcs( array( 'taxonomy' => 'mrc', 'hierarchical' => 1, 'name' => 'search_mrcs', 'orderby' => 'name', 'selected' => $selected_mrc, 'hide_empty' => false ) ); ?>
				<?php else : ?>
					<?php job_manager_dropdown_mrcs( array( 'taxonomy' => 'mrc', 'hierarchical' => 1, 'show_option_all' => __( 'MRC/Villes', 'wp-job-manager' ), 'name' => 'search_mrcs', 'orderby' => 'name', 'selected' => $selected_mrc, 'multiple' => false ) ); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if ( $catprods ) : ?>
			<?php foreach ( $catprods as $category ) : ?>
				<input type="hidden" name="search_catprods[]" value="<?php echo sanitize_title( $category ); ?>" />
			<?php endforeach; ?>
		<?php elseif ( $show_catprods && ! is_tax( 'catprod' ) && get_terms( 'catprod' ) ) : ?>
			<div class="search_catprods">
				<label for="search_catprods"><?php _e( 'Catégories de produits', 'wp-job-manager' ); ?></label>
				<?php if ( $show_category_multiselect ) : ?>
					<?php job_manager_dropdown_catprods( array( 'taxonomy' => 'catprod', 'hierarchical' => 1, 'name' => 'search_catprods', 'orderby' => 'name', 'selected' => $selected_catprod, 'hide_empty' => false ) ); ?>
				<?php else : ?>
					<?php job_manager_dropdown_catprods( array( 'taxonomy' => 'catprod', 'hierarchical' => 1, 'show_option_all' => __( 'Catégories de produits', 'wp-job-manager' ), 'name' => 'search_catprods', 'orderby' => 'name', 'selected' => $selected_catprod, 'multiple' => false ) ); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if ( $servicesofferts ) : ?>
			<?php foreach ( $servicesofferts as $category ) : ?>
				<input type="hidden" name="search_servicesofferts[]" value="<?php echo sanitize_title( $category ); ?>" />
			<?php endforeach; ?>
		<?php elseif ( $show_servicesofferts && ! is_tax( 'serviceoffert' ) && get_terms( 'serviceoffert' ) ) : ?>
			<div class="search_servicesofferts">
				<label for="search_servicesofferts"><?php _e( 'Services offerts', 'wp-job-manager' ); ?></label>
				<?php if ( $show_category_multiselect ) : ?>
					<?php job_manager_dropdown_servicesofferts( array( 'taxonomy' => 'serviceoffert', 'hierarchical' => 1, 'name' => 'search_servicesofferts', 'orderby' => 'name', 'selected' => $selected_serviceoffert, 'hide_empty' => false ) ); ?>
				<?php else : ?>
					<?php job_manager_dropdown_servicesofferts( array( 'taxonomy' => 'serviceoffert', 'hierarchical' => 1, 'show_option_all' => __( 'Services offerts', 'wp-job-manager' ), 'name' => 'search_servicesofferts', 'orderby' => 'name', 'selected' => $selected_serviceoffert, 'multiple' => false ) ); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php do_action( 'job_manager_job_filters_search_jobs_end', $atts ); ?>
	</div>

	<?php do_action( 'job_manager_job_filters_end', $atts ); ?>
</form>

<?php do_action( 'job_manager_job_filters_after', $atts ); ?>

<noscript><?php _e( 'Your browser does not support JavaScript, or it is disabled. JavaScript must be enabled in order to view listings.', 'wp-job-manager' ); ?></noscript>
